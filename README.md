# rule-engine2

It is a blackbox application logic used in embedded systems. The RE2 can be linked as a 
library with default behavior, which can react to the external events, change task 
scheduling priority with be a JSON string. With RE2, the embedded system can have identical 
firmware without upgrading firmware.

That feature benefits the cost-down of development and maintenance. Firmware programming
and upgrading on a narrow-band or embedded systems are not always convinient and cheap.

RE2 seperate the execution logic and business data handling, only data and its settings
will be transmitted.

It was inspirated from other close source rule engine, but re2 is a clean room 
implementation.

## Lighting

Let us assume a lighting scenario. We have one LED light, which has to interact with 
various sensors as following:

- A: LED light with ARGB channels
- B/C: Two switches with 6 keys
- C/D/E: Three PIR sensors indicated if persons are passing the spot areas.
- G: Gateway for remote On/Off

As its default behavior, it should act according to:

- Date: different behavior on workdays and weekdays
- Time: different behavior on duty/off duty
- Holiday: different behaviors.

The more consideration, the more scenarios, the more complex the logic is. 
 
## Sensor and Executor Network

Let us consider another general purpose network from previous lighting sample.

- S1/S2/S3/.../Sn: Different sensors for temp/hum/gas1/gas2/motion/lighting/hall...
- B/C: Two user sensors with 6 keys.
- E/F/G: Buzzer/Relay/Light/Speaker...

Its behavior is extremely complex, and the firmware development, maintenance, upgrading 
are extremely high.

## A General Purpose Application Logic in a Mesh Network

In order to make things simple, we don't involve RTOS here. A baremetal model is used.
Let us compare the GP Logic to a lighting or a sensor data collection application, so
we can figure out the common part of the application logic.

- Remote digital IO edge change event (button press, circuit break, door open/close, user interrupt)
- Remote digital IO level change event (PIR sensor output, gas output over a threshold)
- Internal event (time event, daytime event, illumination event)
- Remote continous level change event (follow actions, such as tuning knobs)
- Output functions (GPIO/PWM/DAC/UART/SPI/I2C/LCD/LED etc.)
- Translation functions (specific purpose conversion.)
