#include <stdio.h>
#include "engine2.h"

// a test external function to override the weak function 
void engine2_on_create(void)
{
    printf("strong functions\r\n");
}

// The main loop of application supported by RE2
int main() {
    unsigned int cnt = 100;

	printf("Rule Engine V2 Demonstration\r\n");
    printf("Total %d times will be executed.\r\n", cnt);

    engine2_init();
    engine2_loop();

	return 0;
}