#ifndef _ENGINE2_H_
#define _ENGINE2_H_

/*
The design is inspired by Google's Android lifecycle, with small
modifications according to the embedded systems.
*/
void engine2_on_create(void);
void engine2_on_start(void);
void engine2_on_restart(void);
void engine2_on_resume(void);
void engine2_on_pause(void);
void engine2_on_stop(void);
void engine2_on_destroy(void);
void engine2_default(void);
void engine2_loop(void);
void engine2_run(void);
void engine2_init(void);

#define params_sys_default  0x01
#define params_status       0x02
#define params_button       0x03    // Digital IO edge change based button press event
#define params_movement     0x04    // PIR movement Digital level change event
#define params_dimmer       0x05    // User enforce PWM output
#define params_calendar     0x06    // Device working on week days
#define params_illuminance  0x07    // Device working on hour/min/sec

#define params_dio_edge     params_button       // Edge @ MAC + Channel + Up/Down
#define params_dio_level    params_movement     // Level @ MAC + Channel + Level
#define params_force_out    params_dimmer       // UserLevel @ MAC + Channel + Level
#define params_work_day     params_calendar     // Time @ start + stop
#define params_work_time    params_illuminance  // Weekday @ start + stop

#endif