#include <stdio.h>
#include "engine2.h"

static unsigned int engine2_active; 
static char incoming[256];
static unsigned char allowed_mac[16][8];
static unsigned char allowed_index[16][8];
static unsigned char allowed_priority[16];
static unsigned char output_channel[8];

void engine2_set_allowed_mac(unsigned char* mac)
{

}

void engine2_set_allowed_index(unsigned char* idx)
{

}

void engine2_set_allowed_priority(unsigned char* prio)
{

}

// Weak functions
// https://zhuanlan.zhihu.com/p/616109439
// The weak functions can be overrided by external functions with same name.

__attribute__((weak)) void engine2_on_create(void)
{
    printf("weak engine2 functions\r\n");
}

__attribute__((weak)) void engine2_on_start(void)
{
    printf("start\r\n");

}

__attribute__((weak)) void engine2_on_restart(void)
{    
    printf("restart\r\n");
}

__attribute__((weak)) void engine2_on_resume(void)
{    
    printf("resume\r\n");
}

__attribute__((weak)) void engine2_on_pause(void)
{    
    printf("pause\r\n");
}

__attribute__((weak)) void engine2_on_stop(void)
{    
    printf("stop\r\n");
}

__attribute__((weak)) void engine2_on_destroy(void)
{
    printf("destroy");
}

void on_receive_evt(void)
{
    printf("rcv\r\n");
}

void on_transmit_evt(void)
{
    printf("xmt\r\n");
}

void on_alarm_evt(void)
{
    printf("alm\r\n");
}

void engine2_output(void)
{    
    printf("output\r\n");
}

void engine2_action(void)
{
    printf("action\r\n");
}

void engine2_default(void)
{
    printf("default\r\n");
}

void engine2_config(unsigned char *params)
{
    printf("cfg\r\n");
}

void engine2_init(void)
{
    printf("init\r\n");
    engine2_active = 0;
}

void engine2_run(void)
{
    //printf("run\r\n");
}

void engine2_loop(void)
{
    engine2_on_create();
    engine2_on_start();
    engine2_on_resume();
    for(;;){
        engine2_run();
    }
    engine2_on_pause();
    engine2_on_stop();
    engine2_on_destroy();
}